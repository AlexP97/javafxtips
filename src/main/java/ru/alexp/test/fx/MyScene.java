package ru.alexp.test.fx;

import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;

public abstract class MyScene extends Scene {

    protected final BorderPane pane;

    public MyScene(BorderPane pane) {
        super(pane);
        this.pane = pane;
        init();
    }

    public MyScene() {
        this(new BorderPane());
    }

    abstract void init();
}
