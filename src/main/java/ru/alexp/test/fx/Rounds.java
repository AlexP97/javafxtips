package ru.alexp.test.fx;

import javafx.animation.Transition;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.util.Duration;

import java.util.ArrayList;

public class Rounds extends MyScene {

    private double minR = 0;
    private double maxR = Math.min(600/2, 800/2);
    private Color[] colors;
    private ArrayList<Ellipse> ellipses;
    private Group group;

    @Override
    void init() {
        colors = fromWeb("f44336", "E91E63", "9C27B0", "673AB7", "3F51B5", "2196F3", "03A9F4", "009688", "4CAF50",
                "8BC34A", "CDDC39", "FFEB3B", "FFC107", "FF9800", "FF5722");
        ellipses = new ArrayList<>();
        group = new Group();

        for (int i = 0; i < colors.length; i++) {
            Ellipse e = new Ellipse();
            e.setFill(colors[i]);
            e.setCenterX(pane.getWidth() / 2);
            e.setCenterY(pane.getHeight() / 2);

            e.setRadiusX(minR);
            e.setRadiusY(minR);

            ellipses.add(e);
        }

        Transition animation = new Transition() {

            double dur;
            double delta;

            {
                setCycleDuration(Duration.millis(colors.length * 700));
                dur = getCycleDuration().toMillis();
                delta = dur / ellipses.size();
            }

            @Override
            protected void interpolate(double t) {
                double time = t * dur;
                for (int i = 0; i < ellipses.size(); i++) {
                    Ellipse e = ellipses.get(i);
                    if (time >= delta * i && time <= delta * (i + 1)) {
                        double state = 1 - Math.pow(2, -16 * ((time - delta * i) / delta));
                        double r = minR + (maxR - minR) * state;
                        e.toFront();
                        e.setRadiusY(r);
                        e.setRadiusX(r);
                    } else if (time >= delta * (i + 2) && time >= delta * (i + 3)) {
                        double state = 1 - Math.pow(2, -16 * ((time - delta * (i + 2)) / delta));
                        double r = maxR - (minR + (maxR - minR) * state);
                        e.setRadiusX(r);
                        e.setRadiusY(r);
                    }
                }
            }
        };

        animation.setOnFinished(event -> animation.play());

        group.getChildren().addAll(ellipses);
        pane.setCenter(group);
        animation.play();
    }

    public Color[] fromWeb(String... web) {
        Color[] cols = new Color[web.length];
        for (int i = 0; i < web.length; i++)
            cols[i] = Color.web(web[i]);
        return cols;
    }
}
