package ru.alexp.test.fx;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class main extends Application {

    public static void main(String... args) {
        Application.launch(main.class, args);
    }

    @Override
    public void start(Stage ps) throws Exception {
        ps.initStyle(StageStyle.UNDECORATED);
        MyScene scene = new Rounds();
        ps.setScene(scene);
        ps.setHeight(600);
        ps.setWidth(800);

        ps.show();
    }

}
